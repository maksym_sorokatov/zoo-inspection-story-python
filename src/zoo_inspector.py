class ZooInspector(object):
    def __init__(self, image_recognition_system, inspection_log):
        self.image_recognition_system = image_recognition_system
        self.inspection_log = inspection_log

    def inspect(self, zoo):
        inspection = Inspection(zoo, self.image_recognition_system)
        inspection.run_inspection()
        self.inspection_log.log(inspection.inspection_statuses)


class Inspection(object):
    WARNING_STATUS = 'WARNING'
    OK_STATUS = 'OK'
    ZOO_STATUS_NAME = 'ZOO'
    ENCLOSURE_STATUS_NAME = 'ENCLOSURE'
    ANIMAL_STATUS_NAME = 'ANIMAL'

    def __init__(self, zoo, image_recognition_system):
        self.zoo = zoo
        self.image_recognition_system = image_recognition_system
        self.inspection_statuses = []

    def run_inspection(self):
        self.inspect_enclosures_and_animals()
        self.report_zoo_status()

    def inspect_enclosures_and_animals(self):
        for enclosure in self.zoo.get_enclosures():
            self.inspect_enclosure(enclosure)
            self.inspect_animal(enclosure)

    def report_zoo_status(self):
        zoo_status = self.WARNING_STATUS if self.is_zoo_in_warning_status() else self.OK_STATUS
        self.report_status(self.ZOO_STATUS_NAME, self.zoo.get_id(), zoo_status)

    def is_zoo_in_warning_status(self):
        return self.is_not_empty_inspection_statuses()

    def is_not_empty_inspection_statuses(self):
        return len(self.inspection_statuses) > 0

    def report_enclosure_warning_status(self, enclosure):
        self.report_status(self.ENCLOSURE_STATUS_NAME, enclosure.get_id(), self.WARNING_STATUS)

    def report_animal_warning_status(self, animal):
        self.report_status(self.ANIMAL_STATUS_NAME, animal.get_name(), self.WARNING_STATUS)
    
    def report_status(self, object_name, object_id, status):
        self.inspection_statuses.append(f'{object_name}#{object_id}#{status}')

    def inspect_enclosure(self, enclosure):
        enclosure_image = self.zoo.capture_picture_of(enclosure)
        enclosure_status = self.image_recognition_system.recognize_enclosure_status(enclosure, enclosure_image)
        if (not enclosure_status.is_enclosure_safe()):
            self.respond_to_not_safe_enclosure(enclosure)
            self.report_enclosure_warning_status(enclosure)
    
    def respond_to_not_safe_enclosure(self, enclosure):
        self.zoo.close_enclosure(enclosure)
        self.zoo.request_security_to(enclosure)
        self.zoo.request_maintenance_crew_to(enclosure)

    def inspect_animal(self, enclosure):
        animal_image = self.zoo.capture_picture_of(enclosure.get_animal())
        animal_status = self.image_recognition_system.recognize_animal_status(enclosure.get_animal(), animal_image)
        if (animal_status.is_animal_sick()):
            self.respond_to_sick_animal(enclosure)
            self.report_animal_warning_status(enclosure.get_animal())

    def respond_to_sick_animal(self, enclosure):
        self.zoo.close_enclosure(enclosure)
        self.zoo.request_veterinary_to(enclosure.get_animal())
